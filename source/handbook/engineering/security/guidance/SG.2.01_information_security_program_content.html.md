---
layout: markdown_page
title: "SG.2.01 - Information Security Program Content Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# SG.2.01 - Information Security Program Content

## Control Statement

The GitLab Director of Security conducts a periodic staff meeting to communicate and align on relevant security threats, program performance, and resource prioritization.

## Context

By holding meetings to communicate information about the security program and relevant security threats, GitLab team-members can better understand GitLab's overall security posture, future initiatives, and the threat landscape. Such meetings also afford an opportunity to engage with and learn more about security, the benefits of which can extend outside the security department and bring value to customers and partners.

## Scope

TBD

## Ownership

GitLab's Director of Security

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SG.2.01_information_security_program_content.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SG.2.01_information_security_program_content.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SG.2.01_information_security_program_content.md).

## Framework Mapping

* ISO
  * A.5.1.1
  * A.6.1.1
  * A.6.1.5
  * A.6.2.1
  * A.6.2.2
  * A.9.1.1
  * A.10.1.1
  * A.11.2.9
  * A.13.2.1
* SOC2 CC
  * CC1.1
  * CC1.2
  * CC1.3
  * CC2.2
  * CC2.3
  * CC3.1
  * CC3.2
  * CC5.1
  * CC5.2
* PCI
  * 1.5
  * 2.5
  * 3.7
  * 4.3
  * 5.4
  * 6.7
  * 7.3
  * 8.1
  * 8.1.1
  * 8.1.2
  * 8.1.3
  * 8.1.4
  * 8.1.5
  * 8.1.6
  * 8.1.7
  * 8.1.8
  * 8.4
  * 8.8
  * 9.10
  * 10.8
  * 10.9
  * 11.6
  * 12.1
  * 12.3
  * 12.3.1
  * 12.3.10
  * 12.3.2
  * 12.3.3
  * 12.3.4
  * 12.3.5
  * 12.3.6
  * 12.3.7
  * 12.3.8
  * 12.3.9
  * 12.4
