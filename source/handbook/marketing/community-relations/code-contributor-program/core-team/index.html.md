---
layout: markdown_page
title: "Core Team"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----
## Becoming a Core Team member

A new member can be added to the [Core Team](https://about.gitlab.com/community/core-team/) at any time through the following steps:

1.  Current Core Team members can nominate a new member from the wider community at any time using a confidential issue in [the Core Team group](https://gitlab.com/groups/gitlab-core-team/-/issues). 
2.  The nominee will be added to the Core Team if they accept the nomination and have received at least two positive votes from the rest of the Core Team members, and there's no objection within a two week period. 

## Monthly Core Team meetings

The Core Team meets once a month and anyone is welcome to join the call. Call logistics/agenda/notes for the meeting are available on the [Core Team meeting page](https://gitlab.com/gitlab-core-team/general/wikis/monthly-core-team-meeting). All meeting recordings are available at the [Core Team meeting Playlist](https://www.youtube.com/playlist?list=PLFGfElNsQthZ12EUkq3N9QlThvkf3WGnZ). 

## Contacting Core Team members

Core Team members can be reached by [mentioning](https://docs.gitlab.com/ee/user/group/subgroups/index.html#mentioning-subgroups) `@gitlab-core-team` in issues or merge requests. 

## Stepping down gracefully 

If you are no longer able to or interested in serving in the Core Team, you should make an annoucement on the `#core` Slack channel. When you step down, you will become a [Core Team Alumni](https://about.gitlab.com/community/core-team/alumni/). Once a Core Team member steps down, GitLab team member(s) will start the off-boarding activities to: 

1.  Move the individual from the `team.yml` file to the `alumni.yml` file.
2.  Remove individual from GitLab Slack, [the Core Team Group](https://gitlab.com/groups/gitlab-core-team/community-members/-/group_members), and gitlab-org. 

