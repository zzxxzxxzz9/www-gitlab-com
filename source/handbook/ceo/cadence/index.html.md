---
layout: markdown_page
title: "Cadence"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Overview

Everything in a company happens in a certain cadence.
The period of each cadence differs.
The timescale between periods are about 4x, varying from 3x to 5x.
Below are the cadences we have at GitLab:

1. Day
1. Week (5 workdays)
1. Month (4.3 weeks)
1. Quarter (3 months)
1. Year (4 quarters)
1. Strategy (3 years)
1. Vision (10 years, 3.3x)
1. Mission (30 years, 3x)

## Day

- Group conversation
- Company call
- Breakout call

## Week

- [1-1 cadence with reports](https://about.gitlab.com/handbook/leadership/1-1/)
- Measure growth of early startup, 10% WoW, [for example Meltano](https://meltano.com/docs/roadmap.html#maui)

## Month

- Metrics call
- Release
- Retrospective
- Most KPIs

## Quarter

- OKRs
- Board meeting
- Sales targets
- E-group offsite

## Year

- Yearly plan
- Most of Direction

## Strategy

3 years

- Most of strategy
- Option vesting after cliff of 1 year is passed
- Average retention of team members is around 3 years, with reduced [turnover](https://about.gitlab.com/handbook/people-operations/people-operations-metrics/#team-member-turnover) (<16%). See [actual reports](https://about.gitlab.com/handbook/people-operations/people-operations-metrics/#reporting)

## Vision

10 years

- Product vision
- Time needed for category creation
- Investor fund time limit
- Commitment of DZ

## Mission

30 years

- Time to realize [our mission](https://about.gitlab.com/company/strategy/#mission)
- [Lifespan of the average company](https://www.bbc.com/news/business-16611040), 10 years to get into the S&P500, then 15 in it, and 5 of decline for a total of 30
- [Lifespan of Amazon](https://www.forbes.com/sites/richardkestenbaum/2018/11/16/amazon-is-not-too-big-to-fail-bezos/#65fba0621626) "Amazon is not too big to fail...In fact, I predict one day Amazon will fail. Amazon will go bankrupt. If you look at large companies, their lifespans tend to be 30-plus years, not a hundred-plus years."
- [Generation is also 30 years](https://www.ncbi.nlm.nih.gov/pubmed/10677323)
