---
layout: markdown_page
title: "Category Vision - Tracing"
---

- TOC
{:toc}

## Tracing
Today we have metrics, which provides information like how often a particular event occurs or the quantity of a resource being consumed. This is great for things like tracking the how long it takes to respond to requests, how much CPU is consumed, etc.

What we are missing is deeper insight into the application. To continue with the example we may know that on average requests are serviced around 350ms. What we don't know is why. Is it a long database call? Is an underlying service slow to respond? Is there a blocking operation causing resource contention?

These questions can be answered with Tracing, which provides an in-depth look at a particular request, tracing its flow from initial reception to response to the service.

## What's Next & Why
We're pursuing continued iteration on our initial [Tracing MVC](https://gitlab.com/groups/gitlab-org/-/epics/89) with a specific next step to [enable deployment of Jaeger to your Kubernetes cluster](https://gitlab.com/gitlab-org/gitlab-ee/issues/5182). 

## Maturity Plan
* [Base Epic](https://gitlab.com/groups/gitlab-org/-/epics/837)

## Competitive Landscape
Not yet, but accepting merge requests to this document.

## Analyst Landscape
Not yet, but accepting merge requests to this document.

## Top Customer Success/Sales Issue(s)
Not yet, but accepting merge requests to this document.

## Top Customer Issue(s)
Not yet, but accepting merge requests to this document.

## Top Internal Customer Issue(s)
Not yet, but accepting merge requests to this document.

## Top Vision Item(s)
Not yet, but accepting merge requests to this document.
